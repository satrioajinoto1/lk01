public class Kereta {
    private String namaKereta;
    private int jumlahPenumpang;
    private int tiketAda;
    private Ticket[] daftarTiket;

    /*
     * Overloading constructor adalah dua constructor berbeda dengan nama yang sama
     * Constructor Kereta Ini Digunakan Jika Kereta Memiliki Satu Parameter Saja
     * Constructor Kereta Komuter Agar Memiliki Nilai Default
     */
    public Kereta() {
        namaKereta = "Komuter";
        tiketAda = 1000;
        daftarTiket = new Ticket[tiketAda];
    }

    /*
     * Constructor Kereta KAJJ
    */
    public Kereta(String namaKereta, int tiketAda) {
        this.namaKereta = namaKereta;
        this.tiketAda = tiketAda;
        this.daftarTiket = new Ticket[this.tiketAda];
    }

    /* 
     * Overloading method yaitu method dengan nama yang sama akan tetapi memiliki parameter berbeda.
     * tambahTiket pertama digunakan untuk membeli tiket dengan parameter namaPenumpang saja (Kereta Komuter)
    */
    public void tambahTiket(String namaPenumpang) {
        Ticket tiket = new Ticket(namaPenumpang);
        if (tiketAda > jumlahPenumpang) {
            daftarTiket[jumlahPenumpang] = tiket;
            jumlahPenumpang++;
            System.out.println("==================================================");
            if (tiketAda - jumlahPenumpang >= 30) {
                System.out.println("Tiket Berhasil Dipesan");
            } else if (tiketAda - jumlahPenumpang < 30) {
                System.out.println("Tiket Berhasil Dipesan Sisa Tiket Yang Tersedia : " + (tiketAda - jumlahPenumpang));
            }
        }else{
            System.out.println("==================================================");
            System.out.println("Kereta telah habis dipesan, silahkan cari jadwal keberangkatan lainnya");
        }
    }
    /* 
     * tambahTiket kedua digunakan untuk membeli tiket dengan parameter namaPenumpang, Asal dan Tujuan (Kereta KAJJ)
    */
    public void tambahTiket(String namaPenumpang, String Asal, String Tujuan) {
        Ticket tiket = new Ticket(namaPenumpang, Asal, Tujuan);
        if (tiketAda > jumlahPenumpang) {
            daftarTiket[jumlahPenumpang] = tiket;
            jumlahPenumpang++;
            System.out.println("==================================================");
            if (tiketAda - jumlahPenumpang >= 30) {
                System.out.println("Tiket Berhasil Dipesan");
            } else if (tiketAda - jumlahPenumpang < 30) {
                System.out.println("Tiket Berhasil Dipesan Sisa Tiket Yang Tersedia : " + (tiketAda - jumlahPenumpang));
            } 
        }else{
            System.out.println("==================================================");
            System.out.println("Kereta telah habis dipesan, silahkan cari jadwal keberangkatan lainnya");
        }
    }
    /* 
     * tampilTiket menunjukan pesanan yang sudah di pesan oleh pembeli
    */
    public void tampilkanTiket() {
        System.out.println("==================================================");
        System.out.println("Daftar penumpang kereta api " + namaKereta + ":");
        System.out.println("-----------------------");
        //Output nama diletakkan diluar agar nilai asal dan tujuan tidak ada pada kereta komuter
        for (int i = 0; i < jumlahPenumpang; i++) {
            System.out.println("Nama    : " + daftarTiket[i].getNama());
            if (daftarTiket[i].getAsal() != null) {
                System.out.println("Asal    : " + daftarTiket[i].getAsal());
                System.out.println("Tujuan  : " + daftarTiket[i].getTujuan());
                System.out.println("-----------------------");
            }
        }
    }

}