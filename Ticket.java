public class Ticket {
    private String namaPenumpang;
    private String Asal;
    private String Tujuan;

    public Ticket(String namaPenumpang) {
        this.namaPenumpang = namaPenumpang;
    }

    public Ticket(String namaPenumpang, String Asal, String Tujuan) {
        this.namaPenumpang = namaPenumpang;
        this.Asal = Asal;
        this.Tujuan = Tujuan;
    }

    public String getNama() {
        return namaPenumpang;
    }

    public String getAsal() {
        return Asal;
    }

    public String getTujuan() {
        return Tujuan;
    }

}